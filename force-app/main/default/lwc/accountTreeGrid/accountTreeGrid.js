import { LightningElement, track } from 'lwc';
import fetchingAccount from '@salesforce/apex/treeGridHelper.fetchingAccount';
export default class AccountTreeGrid extends LightningElement {

    
    @track allAccounts = [];
    length = 0;

    accountDetails = [];
    connectedCallback(){

        fetchingAccount().then(result=>{

            if(result){

                let tempAccountData = [];
                tempAccountData = result;
                this.accountDetails = result;
                this.length = tempAccountData.length;

                console.log(JSON.parse(JSON.stringify(tempAccountData)));
                
                let finalTempList = [];
                tempAccountData.forEach((account)=>{

                    let myPromise = new Promise((resolve,reject)=>{
                        resolve(this.myMethod(account));
                        reject('rejected');
                    });

                    myPromise.then((result)=>{
                        console.log(result);
                        console.log("first account => ", JSON.parse(JSON.stringify(result)));
                        finalTempList.add(result); 
                        console.log("ready List =====> ", JSON.parse(JSON.stringify(finalTempList)));


                    }).catch(error=>{

                        console.log('rejected', reject);
                        console.log("err", error);
                    })
                    
                    
                })

                this.allAccounts = finalTempList;
            }
            
        }).catch(error=>{

            console.log("error => ", JSON.parse(JSON.stringify(error)));
        })
    }

    @track columns = [
        {
            
            label : 'Name',
            fieldName : 'Name',
            type : 'text'
        },
        {
            
            label : 'Employees',
            fieldName : 'NumberOfEmployees',
            type : 'number'
        },
        {
            
            label : 'Phone Number',
            fieldName : 'Phone',
            type : 'text'
        },
        {
            
            label : 'Account Owner',
            fieldName : 'OwnerId',
            type : 'text'
        },
        {
            label : 'Billing City',
            fieldName : 'BillingAddress',
            type : 'text'
        }
    ]

    handleRowSelection(event){

        console.log("row selected");
        console.log(event.detail);
        
    }

    count = 0;

    myMethod(account){

        this.count+=1;
        let accObject = {};
        console.log("counter = >", this.count);
        console.log("account => ", account);

        let owner = {};
        owner = JSON.parse(JSON.stringify(account.Owner));
        console.log("owner => ", owner);
    
        if('BillingAddress' in account){
            
            console.log('billing exists');
            let billing = {};
            billing = JSON.parse(JSON.stringify(account.BillingAddress));

            accObject = {
                'Name' : account.Name,
                'NumberOfEmployees' : account.NumberOfEmployees,
                'Phone' : account.Phone,
                'OwnerId' : owner.FirstName,
                'BillingAddress' : billing.city
            };

            console.log('Object1 => ', JSON.parse(JSON.stringify(accObject)));
            
            let child = [];

            if('ChildAccounts' in account){

                child = JSON.parse(JSON.stringify(account.ChildAccounts));
                console.log('child1 =>',child);
                let childList = [];

                child.forEach((childRecord)=>{
                    console.log('child Id = ', childRecord.Id);
                    let childAccount = {};
                    let myChild = {};
                    let elementIndex = 0;
                    this.accountDetails.forEach((acc)=>{
                        elementIndex+=1;
                        if(acc.Id == childRecord.Id){
                            console.log('ID matched => ', acc.Id);
                            let myPromise1 = new Promise((resolve,reject)=>{
                                resolve(myChild = acc);
                                reject('rejected');
                            });
        
                            myPromise1.then((result)=>{
                                console.log(result);
                                this.accountDetails.splice(elementIndex-1,1);
                                console.log("finalLength=>", this.accountDetails.length);
                            }).catch(error=>{
        
                                console.log('rejected', reject);
                                console.log("err", error);
                            })
                            
                        }
                    })

                    console.log("mychild =>", myChild);
                    childAccount = this.myMethod(myChild);
                    childList.add(childAccount);
                    
                })

                accObject['_children'] = childList;

                console.log('chilren1 => ',JSON.parse(JSON.stringify(accObject['_children'])));

            }else{

                accObject['_children'] = [];
                console.log('chilren2 => ',JSON.parse(JSON.stringify(accObject['_children'])));
            }
            
        }else{
            console.log('billing does not exist');

            accObject = {
                'Name' : account.Name,
                'NumberOfEmployees' : account.NumberOfEmployees,
                'Phone' : account.Phone,
                'BillingAddress' : ''
            };

            console.log('Object2 => ', JSON.parse(JSON.stringify(accObject)));
            
            let child = [];

            if('ChildAccounts' in account){
                console.log('inside child 2');
                child = JSON.parse(JSON.stringify(account.ChildAccounts));
                console.log('child2 =>',child);

                let childList = [];

                child.forEach((childRecord)=>{
                    console.log('child Id = ', childRecord.Id);
                    let childAccount = {};
                    let elementIndex = 0;
                    let myChild = {};
                    this.accountDetails.forEach((acc)=>{
                        elementIndex+=1;
                        if(acc.Id == childRecord.Id){
                            console.log('ID matched => ', acc.Id);
                            let myPromise1 = new Promise((resolve,reject)=>{
                                resolve(myChild = acc);
                                reject('rejected');
                            });
        
                            myPromise1.then((result)=>{
                                console.log(result);
                                this.accountDetails.splice(elementIndex-1,1);
                                console.log("finalLength=>", this.accountDetails.length);

                            }).catch(error=>{
        
                                console.log('rejected', reject);
                                console.log("err", error);
                            })
                        }
                    })
                    console.log("mychild = > ",myChild);
                    childAccount = this.myMethod(myChild);
                    console.log('child added 2');
                    childList.add(childAccount);


                    // if(this.count!=this.length){

                    //     childAccount = this.myMethod(childRecord);
                    //     console.log('child added 2');
                    //     childList.add(childAccount);

                    // }else{
                    //     console.log("same length 2");

                    //     accObject['_children'] = childList;

                    //     console.log('chilren2 => ',JSON.parse(JSON.stringify(accObject['_children'])));
                    //     return;
                    // }
                })

                accObject['_children'] = childList;
                console.log('chilren3 => ',JSON.parse(JSON.stringify(accObject['_children'])));
            }else{

                accObject['_children'] = [];
                console.log('chilren4 => ',JSON.parse(JSON.stringify(accObject['_children'])));
            }
        }
        console.log(JSON.parse(JSON.stringify(accObject)));

        
        return accObject;
        
    }
}