import { LightningElement, wire, track } from 'lwc';
import fetchingOpportunities from '@salesforce/apex/lwcTaskOpportunity.fetchingOpportunities';
import deleteOpportunities from '@salesforce/apex/lwcTaskOpportunity.deleteOpportunities';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import MyModal from 'c/lwcModal';
import { refreshApex } from '@salesforce/apex';
import {NavigationMixin} from 'lightning/navigation';
export default class NavigationExample extends NavigationMixin(LightningElement){

    refreshData = [];
    @track selectedStage = 'All';
    @track allOpportunities = [];

    @wire (fetchingOpportunities, {stage:'$selectedStage'}) myMethod(result){

        this.refreshData = result;
        if(result.data){

            console.log(result);

            let tempOpportunities = JSON.parse(JSON.stringify(result.data));
            console.log(tempOpportunities);

            tempOpportunities.forEach(opportunity=>{

                if(opportunity.Probability == 0){
                    opportunity.showRed = true;
                    opportunity.color = 'red';
                    opportunity.showGreen = false;
                    
                }else if(opportunity.Probability == 100){
                    opportunity.showGreen = true;
                    opportunity.color = 'green';
                    opportunity.showRed = false;

                }else{
                    opportunity.showBlue=true;
                    opportunity.color = 'blue';
                    opportunity.showGreen = false;
                    opportunity.showRed = false;

                }
            })
    
            this.allOpportunities = tempOpportunities;
    
            console.log(JSON.parse(JSON.stringify(this.allOpportunities)));
            
        }else if(result.error){

            console.log(result.error);
        }
     
    }

    get optionsStage() {
        return [
            { label: 'All', value: "All" },
            { label: 'Prospecting', value: "Prospecting" },
            { label: 'Qualification', value: "Qualification" },
            { label: 'Needs Analysis', value: "Needs Analysis" },
            { label: 'Value Proposition', value: "Value Proposition" },
            { label: 'Id. Decision Makers', value: "Id. Decision Makers" },
            { label: 'Perception Analysis', value: "Perception Analysis" },
            { label: 'Proposal/Price Quote', value: "Proposal/Price Quote" },
            { label: 'Negotiation/Review', value: "Negotiation/Review" },
            { label: 'Closed Won', value: "Closed Won" },
            { label: 'Closed Lost', value: "Closed Lost" }
            
        ];
    }

    filterByStage(event){

        console.log(event.detail.value);
        this.selectedStage = event.detail.value;
        refreshApex(this.refreshData);
    }

    viewDetail(event){

        let record = event.currentTarget.dataset.id;
        console.log(record);

        this.pageReference = {
            type :"standard__recordPage",
            attributes : {
                recordId : record,
                actionName : "view"
            }
        }

        this[NavigationMixin.Navigate](this.pageReference);
    }

    deleteRecord(event){

        let record = event.currentTarget.dataset.id;
        console.log(record);
        let records = [];
        records.push(record);
        console.log(records);
        deleteOpportunities({recordIds:records}).then(result=>{

            console.log(result);

            refreshApex(this.refreshData);
            
        }).catch(error=>{

            console.log(error);
        })
    }

    async closePopUp(event){

        let record = event.currentTarget.dataset.id;

        const result = await MyModal.open(
            {
                recordId : record
            }
        )

        refreshApex(this.refreshData);
        
    }
    

        
    
}