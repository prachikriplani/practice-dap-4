import { api } from 'lwc';
import LightningModal from 'lightning/modal';
import updatingRecord from '@salesforce/apex/lwcTaskOpportunity.updatingRecord';
export default class LwcModal extends LightningModal {


    objectApiName = 'Opportunity';
    @api recordId;
    selectedStage = '';


    handleOkay() {

        this.close('okay');
    }

    handleSuccess(){
        console.log("recordId => ", this.recordId);

        let recordObj = 
        {
            'record' : this.recordId, 
            'stage'  : this.selectedStage
        }
        updatingRecord({recordIds:recordObj}).then(result=>{
            console.log(result);

        }).catch(error=>{

            console.log(error);
        })

        this.close('okay');
    }

    get optionsStage() {
        return [

            { label: 'Closed Won', value: "Closed Won" },
            { label: 'Closed Lost', value: "Closed Lost" }
            
        ];
    }

    getValue(event){

        console.log(event.detail.value);
        this.selectedStage = event.detail.value;
    }

}