public class lwcTaskOpportunity {

    @AuraEnabled(cacheable=true)
    public static List<Opportunity> fetchingOpportunities(String stage){
        
        List<Opportunity> opportunityRecords = new List<Opportunity>();
        if(stage == '' || stage == 'All'){

            opportunityRecords = [SELECT Id, Name, StageName, IsClosed, Probability, Amount FROM Opportunity];

        }else{

            opportunityRecords = [SELECT Id, Name, StageName, IsClosed, Probability, Amount FROM Opportunity WHERE StageName = :stage];

        }
        
        return opportunityRecords;
    }

    @AuraEnabled
    public static void deleteOpportunities(List<Id> recordIds){
        
        List<Opportunity> opportunityRecords = [SELECT Id FROM Opportunity WHERE ID IN :recordIds];

        List<Database.DeleteResult> results = Database.delete(opportunityRecords, False);
    }

    @AuraEnabled
    public static void updatingRecord(Map<String,String> recordIds){

        System.debug(recordIds);
        Set<String> keys = recordIds.keySet();
        System.debug(recordIds.get('record'));
        String record = (String) recordIds.get('record');
        List<Opportunity> opportunityRecords = [SELECT Id, StageName FROM Opportunity WHERE ID = :record];

        for(Opportunity opportunityRecord : opportunityRecords){

            opportunityRecord.StageName = recordIds.get('stage');
        }

        List<Database.SaveResult> results = Database.update(opportunityRecords, False);
    }
    
    
}