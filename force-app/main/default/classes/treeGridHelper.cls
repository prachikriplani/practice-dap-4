public class treeGridHelper {
    
    @AuraEnabled
    public static List<Account> fetchingAccount(){
        
        List<Account> accountRecords = [SELECT Name, NumberOfEmployees, Phone, Owner.FirstName, BillingAddress, (SELECT Name, NumberOfEmployees, Phone, Owner.FirstName, BillingAddress FROM ChildAccounts) FROM Account ];

        for(Account accountRecord : accountRecords){

            for(Account acc : accountRecord.ChildAccounts){

                System.debug(acc);
            }
        }

        return accountRecords;
    }
}